config = {
	# Add here your HTTP API bot token, get it from @BotFather
	"bot.api_token": "1854546593:AAFfSqzylMBJ3n8RHZ_U5zAso59LE7CfrdQ",
	# User IDs of users that have full power of the bot, as a list
	"bot.admin_user_ids": [1698093479],

	# Module - CI
	# Read this before continuing: https://github.com/SebaUbuntu/HomeBot/wiki/Module-%7C-CI#variables
	"ci.main_dir": "",
	"ci.channel_id": "-1001595569305",
	"ci.upload_artifacts": False,
	"ci.github_username": "XenonTheInertG",
	"ci.github_token": "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",
	# twrpdtgen script
	"ci.twrpdtgen.github_org": "XenonTheInertG-CI",
	"ci.twrpdtgen.channel_id": "-1001180461534",

	# Library - libadmin
	"libadmin.approved_user_ids": [1698093479],

	# Library - libupload
	"libupload.method": "",
	"libupload.base_dir": "",
	"libupload.host": "",
	"libupload.port": "",
	"libupload.username": "",
	"libupload.password": "",

	# Library - libweather
	# Read this before continuing: https://github.com/SebaUbuntu/HomeBot/wiki/Module-%7C-Weather#variables
	"libweather.api_key": "d7a56a08b871863558317078901e28e2",
	"libweather.temp_unit": "metric",
}
